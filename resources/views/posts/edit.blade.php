@extends('layout')

@section('content')
    <form method="POST" action="{{ route('posts.update', ['post' => $post->id]) }}">
        @csrf
        @method('PUT')
        <p>
            <label for="">Title</label>
            <input type="text" name="title" id="" value="{{ old('title', $post->title) }}">
        </p>
        <p>
            <label for="">Content</label>
            <input type="text" name="content" id="" value="{{ old('content', $post->content) }}">
        </p>
        @if ($errors->any())
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit">Update!</button>
    </form>
@endsection
