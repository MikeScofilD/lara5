@extends('layout')

@section('content')
    @foreach ($posts as $post)
        <p>
        <h3><a href="{{ route('posts.show', ['posts' => $post->id]) }}">{{ $post->title }}</a></h3>
        <a href="{{route('posts.edit', ['post' => $post->id])}}">Edit</a>
        </p>
        {{-- @empty --}}
            <p>No blog post</p>
        {{-- @endempty --}}
    @endforeach
@endsection
